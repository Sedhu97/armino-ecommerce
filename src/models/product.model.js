const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let productSchema = new Schema({
    name: { type: String, required: true },
    featuredImage: { type: String, required: false },
    price: { type: Number, required: true },
    unit: { type: String, required: true },
    status: { type: Boolean, required: true },
    manufacture: { type: String, required: true },
    stock: { type: Number, required: true },
},
{
    collection: 'product'
})
module.exports = mongoose.model('Product', productSchema)

