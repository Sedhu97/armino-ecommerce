const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let cartSchema = new Schema({
    user_id: { type: String, required: true },
    product_id: { type: Schema.Types.ObjectId, required: true, ref: 'product' },
    quantity: { type: Number, required: true },
    offer_id:{type: String,required:false},
    amount:{ type: Number, required: false },
    // product: { type: Schema.Types.ObjectId, required: true, ref: 'product' },
},
{
    collection: 'cart'
})
module.exports = mongoose.model('Cart', cartSchema)

