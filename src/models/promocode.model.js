const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let promocodeSchema = new Schema({
    name: { type: String, required: true },
    promocode: { type: String, required: false },
    discount: { type: Number, required: true },
    validity:{type: Date,required:false},
    status: { type: Boolean, required: true },
},
{
    collection: 'promocode'
})
module.exports = mongoose.model('Promocode', promocodeSchema)

