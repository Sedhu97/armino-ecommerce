const multer = require('multer');
const Product = require("../models/product.model");
const pagination = require("../middlewares/datatools");
const { updateValidation } = require("../validation/schema/product.schema");
const { productValidation } = require("../validation/schema/product.schema");

const storage = multer.diskStorage({destination: (req, file, cb) => {cb(null, './uploads');},
filename: (req, file, cb) => {cb(null, Date.now() + file.originalname);},
});
const upload = multer({ storage: storage }).fields([{name: 'featuredImage', maxCount: 1}]);
function errorHandler(res,dat) {res.json({status:'error',data:dat})}

exports.create = function (req,res,next){
    upload(req,res,err =>{(err)? errorHandler(res,err): setpath(req,res,next)})
}
exports.update = async function (req,res,next) {
    upload(req,res,err =>{(err)? errorHandler(res,err): setpath2(req,res,next)})
    }
async function setpath(req,res,next){


    const products = req.body;
    const imgs = req.files;
    const { error } = await productValidation(products);
    if (error) return res.status(422).send(error.details);
    if (imgs.featuredImage)
    products.featuredImage = `${process.env.HOST_NAME}/${imgs.featuredImage[0].path}`;
    req.body = products
    next()
}
async function setpath2(req,res,next){
    const imgs = req.files;
    const products = req.body;
    const { error } = await updateValidation(products);
    if (error) return res.status(422).send(error.details);
    if (imgs.featuredImage)
    products.featuredImage = `${process.env.HOST_NAME}/${imgs.featuredImage[0].path}`;
// remove old files from server
    const id = req.params.id;
    const kill = await Product.findById(id);
    if (imgs.featuredImage){
    oldpath = kill.featuredImage
    path = oldpath.replace(`${process.env.HOST_NAME}/`,'');
    if (path)
   await pagination.deleteimage(path),(err)=>errorHandler(res,err)
    }
//set new req.body with updated products
    req.body = products
    next()
}


