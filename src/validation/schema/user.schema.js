const Joi = require('@hapi/joi');
const createValidation = data => {
    const schema = Joi.object({
        name: Joi.string().min(4).optional(),
        email:Joi.string().optional(),
        rollno:Joi.number().optional()
    });
    return schema.validate(data);
};
const updateValidation = data => {
    const schema = Joi.object({
        name: Joi.string().min(4).optional(),
    });
    return schema.validate(data);
};
module.exports.createValidation = createValidation;
module.exports.updateValidation = updateValidation;
