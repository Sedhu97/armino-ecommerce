const Joi = require("@hapi/joi");
const promocodeValidation = (data) => {
  const schema = Joi.object({
    name: Joi.string().required(),
    promocode: Joi.any()
      .meta({ swaggerType: "file" })
      .optional()
      .description("Image File")
      .optional(),
    discount: Joi.number().required(),
    validity: Joi.date().optional(),
    status: Joi.boolean().required(),
  });
  return schema.validate(data);
};
const updateValidation = (data) => {
  const schema = Joi.object({
    name: Joi.string().optional(),
    promocode: Joi.any()
      .meta({ swaggerType: "file" })
      .optional()
      .description("Image File")
      .optional(),
    discount: Joi.number().optional(),
    validity: Joi.date().optional(),
    status: Joi.boolean().optional(),
  });
  return schema.validate(data);
};
module.exports.promocodeValidation = promocodeValidation;
module.exports.updateValidation = updateValidation;
