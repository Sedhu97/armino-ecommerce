
const Joi = require('@hapi/joi');
const createValidation = data => {
    const schema = Joi.object({
        user_id: Joi.string().min(4).required(),
        product_id:Joi.string().required(),
        quantity:Joi.number().min(1).required(),
        offer_id:Joi.string().optional(),
        amount:Joi.number().optional()
       
    });
    return schema.validate(data);
};
const updateValidation = data => {
    const schema = Joi.object({
        user_id: Joi.string().min(4).optional(),
        product_id:Joi.string().optional(),
        quantity:Joi.number().optional(),
        offfer_id:Joi.string().optional(),
        amount:Joi.number().optional(),
    });
    return schema.validate(data);
};
module.exports.createValidation = createValidation;
module.exports.updateValidation = updateValidation;
