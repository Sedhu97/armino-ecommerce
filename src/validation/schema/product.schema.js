const Joi = require("@hapi/joi");
const productValidation = (data) => {
  const schema = Joi.object({
    name: Joi.string().required(),
    featuredImage: Joi.any()
      .meta({ swaggerType: "file" })
      .optional()
      .description("Image File")
      .optional(),
    price: Joi.number().required(),
    unit: Joi.string().required(),
    status: Joi.boolean().required(),
    manufacture: Joi.string().required(),
    stock: Joi.number().required(),
  });
  return schema.validate(data);
};
const updateValidation = (data) => {
  const schema = Joi.object({
    name: Joi.string().optional(),
    featuredImage: Joi.any()
      .meta({ swaggerType: "file" })
      .optional()
      .description("Image File")
      .optional(),
    price: Joi.number().optional(),
    unit: Joi.string().optional(),
    status: Joi.boolean().optional(),
    manufacture: Joi.string().optional(),
    stock: Joi.number().optional(),
  });
  return schema.validate(data);
};
module.exports.productValidation = productValidation;
module.exports.updateValidation = updateValidation;
