const express = require('express');
const router = express.Router();

const userRouter = require("./user.route");
const productRouter = require("./product.route")
const promocodeRouter = require("./promocode.route");
const cartRouter = require("./cart.route")


router.use('/users', userRouter );
router.use("/products",productRouter)
router.use("/promocode",promocodeRouter)
router.use("/cart",cartRouter)

module.exports = router;