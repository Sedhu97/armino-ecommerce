const express = require('express');
const router = express.Router();
// const verify = require('../../middlewares/verifyToken');

const cartController = require('../../controllers/cart.controller');

router.post('/create', cartController.create);
router.get('/all',cartController.list);
router.get('/show/:id',cartController.show);
router.get('/mycart/:uid',cartController.mycart);
router.put('/update/:id',cartController.update);
router.delete('/delete/:id', cartController.delete);

module.exports = router;