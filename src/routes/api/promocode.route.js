const express = require('express');
const router = express.Router();
// const verify = require('../../middlewares/verifyToken');

const promocodeController = require('../../controllers/promocode.controller');

router.post('/create', promocodeController.create);
router.get('/all',promocodeController.list);
router.put('/update/:id',promocodeController.update);
router.delete('/delete/:id', promocodeController.delete);

module.exports = router;