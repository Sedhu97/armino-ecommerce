const Cart = require("../models/cart.model");
const pagination = require("../middlewares/datatools");

// service for get all  data
exports.list = async function (page, limit, filter, sort) {
  try {
    const cart = await pagination.paginate(page,limit,filter,sort,Cart);
    // const  product = await   Product.find({});
    return cart;
  } catch (e) {
    throw Error("Error while getting all data  ");
  }
};
// service for get data using id
exports.show = async function (id) {
  try {
    const cart = await Cart.findById(id);
    return cart;
  } catch (e) {
    throw Error("Error while finding data with ID ");
  }
};
// service for post data
exports.create = async function (cart) {
  try {
    const dat = await new Cart(cart);
    await dat.save();
    const final =await Cart.aggregate([
        //{$group : { _id: "$user_id", AverageBillValue: { "$avg": { "$ifNull": ["$sub_total",0 ] } },count:{ $sum:1}}},
       {$lookup: {from: "product", localField: "product_id", foreignField: "_id", as: "myproduct"}},
        {$match: {myproduct: {$ne: []}}},
        {
         $replaceRoot: { newRoot: { $mergeObjects: [ { $arrayElemAt: [ "$myproduct", 0 ] }, "$$ROOT" ] } }
      },
    //  { $project: {details: 0,_id:0,__v:0 ,count:0},
     { $project: {myproduct: 0,_id:0,__v:0},
     }
       ]);
       const myid=dat.user_id
    var data = final.filter(obj => {
        obj.total = obj.price*obj.quantity;
        return obj.user_id === myid
      });
      const picked_total = data.map((o) => o.total);
      const my_grand_total = picked_total.reduce(function (a, b) {
        return a + b;
      });
      const count = data.length
      let metadata ={};
       metadata.Total = my_grand_total
       metadata.total_items = count;
    return {metadata,data};
    
  } catch (error) {
      console.log("hiii",error)
    throw Error("Error while Posting data ");
  }
};
// service for update data using id
exports.update = async function (id, cart) {
  try {
    const dat = await Cart.findByIdAndUpdate(id, cart, { new: true });
    return dat;
  } catch (error) {
    throw Error("Error while updating data ");
  }
};
// service for delete data using id
exports.delete = async function (id) {
  try {
// delete database details
    const dat = await Cart.findByIdAndRemove(id);
    return dat;
  } catch (error) {
    throw Error("Error while deleting data");
  }
};

exports.mycart = async function (userid) {
  try {
  //   const cart = await Cart.findById(userid);
  //   return cart;

  const final =await Cart.aggregate([
    //{$group : { _id: "$user_id", AverageBillValue: { "$avg": { "$ifNull": ["$sub_total",0 ] } },count:{ $sum:1}}},
   {$lookup: {from: "product", localField: "product_id", foreignField: "_id", as: "myproduct"}},
    {$match: {myproduct: {$ne: []}}},
    {
     $replaceRoot: { newRoot: { $mergeObjects: [ { $arrayElemAt: [ "$myproduct", 0 ] }, "$$ROOT" ] } }
  },
//  { $project: {details: 0,_id:0,__v:0 ,count:0},
 { $project: {myproduct: 0,_id:0,__v:0},
 }
   ]);
   const myid= userid
var data = final.filter(obj => {
    obj.total = obj.price*obj.quantity;
    return obj.user_id === myid
  });
  const picked_total = data.map((o) => o.total);
  const my_grand_total = picked_total.reduce(function (a, b) {
    return a + b;
  });
  const count = data.length
  let metadata ={};
   metadata.Total = my_grand_total
   metadata.total_items = count;
return {metadata,data};

} catch (error) {
  console.log("hiii",error)
throw Error("Error while Posting data ");
}
};




// exports.mycart = async function (userid) {
//     try {
//     //   const cart = await Cart.findById(userid);
//     //   return cart;

//     const final =await Cart.aggregate([
//         //{$group : { _id: "$user_id", AverageBillValue: { "$avg": { "$ifNull": ["$sub_total",0 ] } },count:{ $sum:1}}},
//     //    {$lookup: {from: "product", localField: "product", foreignField: "_id", as: "myproduct"}},

//        {$lookup: {from: "product", localField: "product_id", foreignField: "_id", as: "myproduct"}},
//         {$match: {myproduct: {$ne: []}}},
//         {
//          $replaceRoot: { newRoot: { $mergeObjects: [ { $arrayElemAt: [ "$myproduct", 0 ] }, "$$ROOT" ] } }
//       },
//       { $project: {myproduct: 0},
//      },
//     //   {$lookup: {from: "promocode", localField: "offer_id", foreignField: "_id", as: "mypromo"}},
//     //   {$match: {mypromo: {$ne: []}}},
//     //   {
//     //    $replaceRoot: { newRoot: { $mergeObjects: [ { $arrayElemAt: [ "$mypromo", 0 ] }, "$$ROOT" ] } }
//     // },
// //     { $project: {myproduct: 0,_id:0,__v:0},
// // },
//     //   {   $unwind:"$mypromo" },

//     //  { $project: {details: 0,_id:0,__v:0 ,count:0},
     
//        ]);
//        console.log('jkj',final)
//     //    const myid=userid
//     // var data = final.filter(obj => {
//     //     obj.total = obj.price*obj.quantity;
//     //     return obj.user_id === myid
//     //   });
//     //   const picked_total = data.map((o) => o.total);
//     //   const my_grand_total = picked_total.reduce(function (a, b) {
//     //     return a + b;
//     //   });
//     //   const count = data.length
//     //   let metadata ={};
//     //    metadata.Total = my_grand_total
//     //    metadata.total_items = count;
//     // return {metadata,data};
//     return final;

//     } catch (e) {
//       throw Error("Error while finding data with ID ");
//     }
//   };