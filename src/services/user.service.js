const User = require("../models/user.model");
const jwt = require("jsonwebtoken");
const secret = process.env.TOKEN_SECRET;

//create
exports.create = async function( user,res){
    try{
        const dat = await new User(user)
        await dat.save();
        return dat;
    }
    catch(e){
        throw Error;
    }
}

exports.list = async function(res)
{
    try{
          const dat = await User.find({ });
          return dat;
    }
    catch(e){
        throw Error;
    }
}

exports.update = async function(id,user,res){
    try{
        const dat = await  User.findByIdAndUpdate(id,user);
        return dat;
        }
        catch(e){
            throw Error;
        }
}

exports.delete = async function(id,res){
    try{
        const dat = await User.findByIdAndDelete(id)
        return dat;
    }
    catch(e){
        throw Error;
    }
}

exports.login =async function(user,res){
    try{
        phno = user.phone;
        const check = await User.findOne({ phone: phno });
        password = check.password;
                 if (user.password !== check.password)
         throw Error('invalid password');
         const payload = { id: check.id, username: check.username };
         const token = jwt.sign(payload, secret, { expiresIn: 36000 });
                  
           return {
            _id: check._id,
            username: check.username,
            user_authentication: "sucess",
            token,
          };
        } catch (e) {
          return res.json({ status: "error", message: e.message });
        
    }
}


