const userService =require('../services/user.service');

exports.create =async function(req,res){
    const user = req.body;
    console.log(user,'jewel')
    try{
     const dat = await userService.create(user,res)
     return res.json({data:dat})
    }
    catch(e){
   return res.json({status:"error",msg:e.message})
    }
}

exports.list = async function(req,res){
    try{
        const dat = await userService.list(res)
        return res.json({data:dat})
    }
    catch(e){
        return res.json({status:"error",msg:e.message})
    }
}    

    exports.update = async function(req,res){
       
        try{
            const id = req.params.id
            const user = req.body
            const dat = await userService.update(id,user,res)
            return res.json({data:dat})
        }
        catch(e){
            return res.json({status:"error",msg:e.message})
        }
    }

    exports.delete = async function(req,res){
        try{
            const id = req.params.id
            const dat = await userService.delete(id,res)
            return res.json({data:dat})
        }
        catch(e){
            return res.json({status:"error",msg:e.message})
        }
    }

    exports.login = async function(req,res){
        try{
            const user = req.body
            const dat = await userService.login(user,res)
            return res.json({data:dat})
        }
        catch(e){
            return res.json({status:"error",msg:e.message})
        }
    }
