const promocodeService = require("../services/promocode.service");
const {promocodeValidation} = require("../validation/schema/promocode.schema");
const { updateValidation } = require("../validation/schema/promocode.schema");

exports.list = async function (req, res, next) {
    try {
        const promo = await promocodeService.list(res);
        return res.json({ status: "success", data: promo });
    } catch (e) {
        return res.json({ status: "error", message: e.message });
    }
};
// api for getting data using id

exports.show = async function (req, res, next) {
    const id = req.params.id;

    try {
        const promo = await promocodeService.show(id, res);
        return res.json({ status: "success", data: promo });
    } catch (e) {
        return res.json({ status: "error", message: e.message });
    }
};

// api for posting
exports.create = async function (req, res, next) {
    const promo = req.body;

    try {
        const { error } = await promocodeValidation(promo);

        if (error) return res.json({ status: "error", message: error.message })
        dat = await promocodeService.create(promo, res);
        return res.json({ status: "success", data: dat });
    } catch (e) {
        return res.json({ status: "error", message: e.message });
    }
};

// api for update
exports.update = async function (req, res, next) {
    try {
        const id = req.params.id;
        const promo= req.body;
        const { error } = await updateValidation(promo);

        if (error) return res.json({ status: "error", message: error.message })
        data = await promocodeService.update(id, promo, res);
        return res.json({ status: "success", data: data });
    } catch (e) {
        return res.json({ status: "error", message: e.message });
    }
};
// api for delete
exports.delete = async function (req, res, next) {
    const id = req.params.id;

    try {
        const dat = await promocodeService.delete(id, res);

        return res.json({ status: "success", data: dat });
    } catch (e) {
        return res.json({ status: "error", message: e.message });
    }
};